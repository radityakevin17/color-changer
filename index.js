
const randomNumber = () =>{
  return Math.floor(Math.random() * 11)
}

function changeBackgroundColor(){
  if(randomNumber() === 0){
    body.style.backgroundColor = 'red'
    clickMe.innerText = `it's red!!!`
  }else if(randomNumber() === 1){
    body.style.backgroundColor = 'blue'
    clickMe.innerText = `it's blue!!!`
  }else if(randomNumber() === 3){
    body.style.backgroundColor = 'green'
    clickMe.innerText = `it's green!!!`
  }else if(randomNumber() === 4){
    body.style.backgroundColor = 'honeydew'
    clickMe.innerText = `it's honeydew!!!`
  }
  else if(randomNumber() === 5){
    body.style.backgroundColor = 'aqua'
    clickMe.innerText = `it's aqua!!!`
  }
  else if(randomNumber() === 6){
    body.style.backgroundColor = 'bisque'
    clickMe.innerText = `it's bisque!!!`
  }else if(randomNumber() === 7){
    body.style.backgroundColor = 'cyan'
    clickMe.innerText = `it's cyan!!!`
  }
  else if(randomNumber() === 8){
    body.style.backgroundColor = 'crimson'
    clickMe.innerText = `it's crimson!!!`
  }
  else if(randomNumber() === 9){
    body.style.backgroundColor = 'goldenrod'
    clickMe.innerText = `it's goldenrod!!!`

  }
  else if(randomNumber() === 10){
    body.style.backgroundColor = 'indigo'
    clickMe.innerText = `it's indigo!!!`
  }
}




const body = document.getElementById('body')
const clickMe = document.getElementById('click_me')


clickMe.addEventListener('click', () => changeBackgroundColor())




